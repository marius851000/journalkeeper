use crate::{SimpleDate, Tag, TagStore};
use serde::{Deserialize, Serialize};
use std::io::{Read, Write};

#[derive(Serialize, Deserialize, Default)]
pub struct DayEntry {
    pub entries: Vec<ActionEntry>,
}

impl DayEntry {
    pub fn from_reader<T: Read>(reader: &mut T) -> anyhow::Result<Self> {
        let result: Self = serde_json::from_reader(reader)?;
        Ok(result)
    }

    pub fn to_writer<T: Write>(&self, writer: &mut T) -> anyhow::Result<()> {
        serde_json::to_writer(writer, &self)?;
        Ok(())
    }

    pub fn pretty_print(&self, tag_store: &TagStore) {
        if self.entries.is_empty() {
            println!("No entry for this day.")
        } else {
            for (count, entry) in self.entries.iter().enumerate() {
                let tags =
                    tag_store.solve_multiple_implication(&entry.tags.iter().collect::<Vec<&Tag>>());
                let description_text = if let Some(desc) = &entry.text {
                    desc
                } else {
                    "No description"
                };
                let tags_text = if tags.is_empty() {
                    "No tags".to_string()
                } else {
                    let mut result = String::new();
                    for (count, tag) in tags.iter().enumerate() {
                        if count != 0 {
                            result.push_str(", ");
                        };
                        result.push_str(tag.get_str());
                    }
                    result
                };
                println!("{}. {} ({})", count, description_text, tags_text)
            }
        }
    }
}

pub struct DayEntryWithDate {
    date: SimpleDate,
    pub entry: DayEntry,
}

impl DayEntryWithDate {
    pub fn new(date: SimpleDate, entry: DayEntry) -> Self {
        Self { date, entry }
    }

    pub fn get_date(&self) -> &SimpleDate {
        &self.date
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ActionEntry {
    pub text: Option<String>,
    pub tags: Vec<Tag>,
}
