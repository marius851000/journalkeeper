use anyhow::Context;
use clap::{Parser, Subcommand};
use directories::ProjectDirs;
use journalkeeper::{JournalStore, SimpleDate, Tag, TagInfo};
use std::path::PathBuf;
use journalkeeper::ActionEntry;
//TODO: documentation
//TODO: do not make the id of an action entry change even if one has already been added
//TODO: add an history, to cancel actions. Implying creating some abstraction for performed action, that can be serialized and backup the previous state of changed stuff.

#[derive(Parser, Debug)]
#[command(name = "JournalKeeper")]
#[command(about = "Allow to keep a journal of daily activites with tags")]
struct Args {
    store_path: Option<PathBuf>,
    #[command(subcommand)]
    cmdcategory: SubCategory,
}

#[derive(Subcommand, Debug, Clone)]
enum SubCategory {
    Entry {
        date: Option<String>,
        #[command(subcommand)]
        subcmd: EntrySubCmd,
    },
    Tag {
        tag: String,
        #[command(subcommand)]
        subcmd: TagSubCmd
    }
}

#[derive(Subcommand, Debug, Clone)]
enum EntrySubCmd {
    Add { text: String, tags: Vec<String> },
    Delete { id: usize },
    View {},
}

#[derive(Subcommand, Debug, Clone)]
enum TagSubCmd {
    Create { description: String, implies: Vec<String> },
    SetDescription { description: String },
    AddImplication { implication: String },
    RemoveImplication { implication: String },
    Delete {}
}

//TODO: this is messy and ugly. Should be rewritten someday, once the backend has been enhanced itself.
pub fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    let store_path = if let Some(path) = args.store_path {
        path
    } else {
        ProjectDirs::from("", "", "JournalKeeper").context("Can’t get the the application directory! Is an home directory defined? You can configure the store path yourself instead too.")?.data_dir().to_path_buf()
    };

    let mut journal_store = JournalStore::new(store_path.clone())
        .with_context(|| format!("Can’t load the store at {:?}", &store_path))?;

    match args.cmdcategory {
        SubCategory::Entry { date, subcmd } => {
            let simple_date = if let Some(date_str) = date {
                SimpleDate::from_str(&date_str).with_context(|| "The provided date is invalid")?
            } else {
                SimpleDate::today()
            };
            let mut day_entry = journal_store.get_day(simple_date)?;
            match subcmd {
                EntrySubCmd::Add { tags, text } => {
                    let tags: Vec<Tag> = tags.into_iter().map(Tag::new).collect();
                    for entry in day_entry.entry.entries.iter() {
                        if entry.tags == tags {
                            println!("Warning: Another entry for this day have the same set of tags!");
                        };
                    };
                    for tag in tags.iter() {
                        if !journal_store.tag_store.tags.contains_key(tag) {
                            println!("Information for {:?} missing", tag);
                        }
                    }
                    day_entry.entry.entries.push(ActionEntry {
                        text: Some(text),
                        tags
                    });
                    journal_store.write_day(day_entry).unwrap();
                    println!("New entry added!");
                },
                EntrySubCmd::Delete { id } => {
                    let poped = day_entry.entry.entries.remove(id);
                    journal_store.write_day(day_entry).unwrap();
                    println!("Deleted {:?} ", poped);
                }
                EntrySubCmd::View {} => day_entry.entry.pretty_print(&journal_store.tag_store),
            }
        },
        SubCategory::Tag{tag, subcmd} => {
            let tag = Tag::new(tag);
            match subcmd {
                TagSubCmd::Create {description, implies} => {
                    journal_store.tag_store.tags.insert(tag, TagInfo {
                        description: Some(description),
                        implies: implies.into_iter().map(Tag::new).collect()
                    });
                    journal_store.save_tag_store()?;
                    println!("Tag info added!")
                },
                TagSubCmd::SetDescription {description} => {
                    let old_description = journal_store.tag_store.tags.get(&tag).unwrap().description.clone();
                    journal_store.tag_store.tags.get_mut(&tag).unwrap().description = Some(description);
                    journal_store.save_tag_store()?;
                    if let Some(old_description) = old_description {
                        println!("Description replaced (old one was \"{}\")", old_description);
                    } else {
                        println!("Description set!")
                    }
                },
                TagSubCmd::AddImplication {implication} => {
                    let implication = Tag::new(implication);
                    journal_store.tag_store.tags.get_mut(&tag).unwrap().implies.insert(implication);
                    journal_store.save_tag_store()?;
                    println!("Implication added")
                },
                TagSubCmd::RemoveImplication {implication} => {
                    let implication = Tag::new(implication);
                    if journal_store.tag_store.tags.get_mut(&implication).unwrap().implies.remove(&tag) {
                        journal_store.save_tag_store()?;
                        println!("implication removed!");
                    } else {
                        println!("The implication was not present in the tag info");
                    }
                },
                TagSubCmd::Delete {} => {
                    if journal_store.tag_store.tags.remove(&tag).is_none() {
                        println!("The tag info was not present");
                    } else {
                        journal_store.save_tag_store()?;
                        println!("Tag info removed!")
                    }
                }
            }
        }
    }

    return Ok(());
}
