use std::collections::HashSet;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Debug, PartialOrd, Ord)]
pub struct Tag(String);

impl Tag {
    pub fn new(key: String) -> Self {
        Self(key)
    }

    pub fn get_str(&self) -> &str {
        &self.0
    }
}

#[derive(Serialize, Deserialize)]
pub struct TagInfo {
    pub implies: HashSet<Tag>,
    pub description: Option<String>,
}

impl Default for TagInfo {
    fn default() -> Self {
        Self { implies: HashSet::new(), description: Default::default() }
    }
}