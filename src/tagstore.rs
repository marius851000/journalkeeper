use crate::Tag;
use crate::TagInfo;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize, Default)]
pub struct TagStore {
    pub tags: BTreeMap<Tag, TagInfo>,
}

impl TagStore {
    pub fn solve_implication<'t>(&'t self, tag: &'t Tag) -> Vec<&'t Tag> {
        let mut result = Vec::with_capacity(10);
        self.solve_implication_inner(tag, &mut result);
        result
    }

    pub fn solve_multiple_implication<'t>(&'t self, tags: &[&'t Tag]) -> Vec<&'t Tag> {
        let mut result = Vec::with_capacity(20);
        for tag in tags {
            self.solve_implication_inner(tag, &mut result);
        }
        result
    }

    fn solve_implication_inner<'t>(&'t self, tag: &'t Tag, result: &mut Vec<&'t Tag>) {
        if result.contains(&tag) {
            println!(
                "circular dependancies found for {:?}. One of those depends on it: {:?}.",
                tag, result
            );
            return;
        }
        result.push(tag);
        if let Some(taginfo) = self.tags.get(tag) {
            for implication in &taginfo.implies {
                self.solve_implication_inner(implication, result);
            }
        }
    }
}
