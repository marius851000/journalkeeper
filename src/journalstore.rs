use crate::{atomic_write_anyhow, DayEntry, DayEntryWithDate, SimpleDate, TagStore};
use anyhow::Context;
use atomicwrites::{AllowOverwrite, AtomicFile};
use std::fs::{create_dir_all, File};
use std::io::ErrorKind;
use std::path::{Path, PathBuf};

pub struct JournalStore {
    root_path: PathBuf,
    pub tag_store: TagStore,
}

impl JournalStore {
    pub fn new(root_path: PathBuf) -> anyhow::Result<Self> {
        let tag_store = Self::get_tag_store(&root_path)?;
        Ok(Self {
            root_path,
            tag_store,
        })
    }

    fn get_day_path(&self, date: &SimpleDate) -> PathBuf {
        let mut path = self.root_path.join(date.year.to_string());
        path.push(date.month.to_string());
        let mut file_name = date.day.to_string();
        file_name.push_str(".json");
        path.push(file_name);
        path
    }

    pub fn get_day(&self, date: SimpleDate) -> anyhow::Result<DayEntryWithDate> {
        let path = self.get_day_path(&date);
        let day_entry = match File::open(&path) {
            Ok(mut file) => DayEntry::from_reader(&mut file)
                .with_context(|| format!("Can’t parse the journal file at {:?}", &path))?,
            Err(err) => match err.kind() {
                ErrorKind::NotFound => DayEntry::default(),
                _ => {
                    return Err(err)
                        .with_context(|| format!("Can’t open the journal file at {:?}", &path))
                }
            },
        };
        Ok(DayEntryWithDate::new(date, day_entry))
    }

    pub fn write_day(&self, day_entry_with_date: DayEntryWithDate) -> anyhow::Result<()> {
        let path = self.get_day_path(day_entry_with_date.get_date());
        if let Some(folder) = path.parent() {
            create_dir_all(folder).with_context(|| {
                format!(
                    "Can’t (recursively) create the directory at {:?}",
                    path.parent()
                )
            })?;
        }
        let atomic_file = AtomicFile::new(&path, AllowOverwrite);
        atomic_write_anyhow(
            atomic_file,
            |file| {
                day_entry_with_date.entry.to_writer(file).with_context(|| {
                    format!(
                        "Can’t write the journal file (with serde_json) at {:?}",
                        &path
                    )
                })
            },
            &path,
        )?;
        Ok(())
    }

    fn get_tag_store_path(root_path: &Path) -> PathBuf {
        root_path.join("tagstore.json")
    }

    fn get_tag_store(root_path: &Path) -> anyhow::Result<TagStore> {
        let path = Self::get_tag_store_path(root_path);
        Ok(match File::open(&path) {
            Ok(mut file) => serde_json::from_reader(&mut file).with_context(|| {
                format!(
                    "Can’t parse the tag info file at {:?} with serde_json",
                    &path
                )
            })?,
            Err(err) => match err.kind() {
                ErrorKind::NotFound => TagStore::default(),
                _ => {
                    return Err(err)
                        .with_context(|| format!("Can’t open the tag info file at {:?}", &path))?
                }
            },
        })
    }

    pub fn save_tag_store(&self) -> anyhow::Result<()> {
        let path = Self::get_tag_store_path(&self.root_path);
        if let Some(folder) = path.parent() {
            create_dir_all(folder).with_context(|| {
                format!(
                    "Can’t (recursively) create the directory at {:?}",
                    path.parent()
                )
            })?;
        }
        let atomic_file = AtomicFile::new(&path, AllowOverwrite);
        atomic_write_anyhow(
            atomic_file,
            |file| {
                serde_json::to_writer(file, &self.tag_store).with_context(|| {
                    format!(
                        "Can’t write the tag info file (with serde_json) at {:?}",
                        &path
                    )
                })
            },
            &path,
        )?;
        Ok(())
    }
}
