mod journalstore;
pub use journalstore::JournalStore;

mod entries;
pub use entries::{ActionEntry, DayEntry, DayEntryWithDate};

mod tag;
pub use tag::{Tag, TagInfo};

mod tagstore;
pub use tagstore::TagStore;

use anyhow::Context;
use atomicwrites::AtomicFile;
use chrono::Datelike;
use std::fs::File;
use std::path::Path;
//TODO: Switch to a chrono data structure
#[derive(Debug)]
pub struct SimpleDate {
    day: u8,
    month: u8,
    year: u32,
}

impl SimpleDate {
    pub fn from_str(text: &str) -> Option<Self> {
        let mut part_iter = text.split('-');
        //TODO: ugly
        let part1 = if let Some(s) = part_iter.next() {
            if let Ok(s) = s.parse() {
                s
            } else {
                return None;
            }
        } else {
            return None;
        };
        let part2 = if let Some(s) = part_iter.next() {
            if let Ok(s) = s.parse() {
                s
            } else {
                return None;
            }
        } else {
            return None;
        };
        let part3 = if let Some(s) = part_iter.next() {
            if let Ok(s) = s.parse() {
                s
            } else {
                return None;
            }
        } else {
            return None;
        };
        Some(SimpleDate {
            day: part1,
            month: part2,
            year: part3,
        })
    }

    pub fn today() -> Self {
        let date = chrono::offset::Local::today().naive_local();
        Self {
            day: date.day().try_into().unwrap(),
            month: date.month().try_into().unwrap(),
            year: date.year().try_into().unwrap(),
        }
    }
}

pub fn atomic_write_anyhow<R, F: FnOnce(&mut File) -> Result<R, anyhow::Error>>(
    atomic_file: AtomicFile,
    f: F,
    path: &Path,
) -> Result<R, anyhow::Error> {
    match atomic_file.write(f) {
        Ok(r) => Ok(r),
        Err(innererr) => match innererr {
            atomicwrites::Error::Internal(err) => {
                return Err(err)
                    .with_context(|| format!("Can’t handle the atomic part of {:?}", path))
            }
            atomicwrites::Error::User(err) => {
                return Err(err)
                    .with_context(|| format!("Can’t write to the atomic side of {:?}", path))
            }
        },
    }
}
